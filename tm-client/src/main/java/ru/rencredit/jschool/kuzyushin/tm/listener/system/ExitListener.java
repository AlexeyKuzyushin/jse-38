package ru.rencredit.jschool.kuzyushin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;

@Component
public final class ExitListener extends AbstractListener {

    @NotNull
    @Override
    public String name()  {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description()   {
        return "Close application";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.exit(0);
    }
}
