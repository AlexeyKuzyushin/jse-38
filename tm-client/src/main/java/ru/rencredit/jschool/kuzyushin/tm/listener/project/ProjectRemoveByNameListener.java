package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByNameListener extends AbstractListener {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectRemoveByNameListener(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    @EventListener(condition = "@projectRemoveByNameListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable SessionDTO sessionDTO = sessionService.getCurrentSession();
        projectEndpoint.removeProjectByName(sessionDTO, name);
        System.out.println("[OK]");
    }
}
