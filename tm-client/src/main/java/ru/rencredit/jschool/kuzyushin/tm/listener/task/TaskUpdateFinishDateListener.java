package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Date;
import java.text.ParseException;
import java.util.GregorianCalendar;

@Component
public final class TaskUpdateFinishDateListener extends AbstractListener {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskUpdateFinishDateListener(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-finish-date";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update finish date of task";
    }

    @Override
    @EventListener(condition = "@taskUpdateFinishDateListener.name() == #event.command")
    public void handler(final ConsoleEvent event) throws DatatypeConfigurationException {
        System.out.println("[UPDATE FINISH DATE]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER DATE (dd-MMM-yyyy):");
        final Date newDate = Date.valueOf(TerminalUtil.nextLine());
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(newDate);
        final XMLGregorianCalendar xmlGregDate =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        taskEndpoint.updateTaskFinishDate(sessionDTO, id, xmlGregDate);
        System.out.println("[OK]");
    }
}
