package ru.rencredit.jschool.kuzyushin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class UserUpdatePasswordListener extends AbstractListener {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserUpdatePasswordListener(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    @EventListener(condition = "@userUpdatePasswordListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UPDATE USER PASSWORD]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        userEndpoint.updateUserPassword(sessionDTO, password);
        System.out.println("[OK]");
        sessionEndpoint.closeSession(sessionDTO);
        System.out.println("[PLEASE SIGN IN YOUR PROFILE AGAIN]");
    }
}
