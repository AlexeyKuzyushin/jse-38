package ru.rencredit.jschool.kuzyushin.tm.exception.empty;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }
}
