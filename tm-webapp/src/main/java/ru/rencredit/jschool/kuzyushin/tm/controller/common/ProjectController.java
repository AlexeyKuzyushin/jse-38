package ru.rencredit.jschool.kuzyushin.tm.controller.common;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectController(
            final @NotNull IUserService userService,
            final @NotNull IProjectService projectService,
            final @NotNull ITaskService taskService,
            final @NotNull IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectRepository = projectRepository;
    }

    @GetMapping("")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-list");
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-create");
        modelAndView.addObject("users", userService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("project") final @NotNull Project project,
            @RequestParam(value = "userId") final @NotNull  String userId
    ) {
        projectService.create(userId, project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") final @NotNull String id) {
        projectService.removeProjectById(id);
        return "redirect:/projects";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable(value = "id") final @NotNull String id) {
        @Nullable final Project project = projectService.findProjectById(id);
        return new ModelAndView("/project/project-update", "project", project);
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("project") final @NotNull Project project) {
        projectRepository.save(project);
//        projectService.updateProjectById(project.getId(),
//                project.getName(), project.getDescription());
        return "redirect:/projects";
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") @NotNull final String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-view");
        modelAndView.addObject("project", projectService.findProjectById(id));
        modelAndView.addObject("tasks", taskService.findAllByProjectId(id));
        return modelAndView;
    }
}
