package ru.rencredit.jschool.kuzyushin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.bootstrap.ServerBootstrap;
import ru.rencredit.jschool.kuzyushin.tm.config.AppConfiguration;

public class AppServer {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfiguration.class);
        @NotNull final ServerBootstrap serverBootstrap = context.getBean(ServerBootstrap.class);
        serverBootstrap.run();
    }
}
