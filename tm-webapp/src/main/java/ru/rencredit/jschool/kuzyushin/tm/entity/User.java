package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TEXT",
            unique = true,
            nullable = false)
    private String login;

    @NotNull
    @Column(columnDefinition = "TEXT",
            nullable = false)
    private String passwordHash;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String email;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String firstName;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String lastName;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String middleName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    @Column
    private Boolean locked = false;

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects= new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessions = new ArrayList<>();

}
