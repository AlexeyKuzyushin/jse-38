package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TEXT",
            nullable = false)
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    @Column
    private Date startDate;

    @Nullable
    @Column
    private Date finishDate;

    @Nullable
    @Column(updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    @ManyToOne
    private User user;
}
