package ru.rencredit.jschool.kuzyushin.tm.controller.common;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public TaskController(
            final @NotNull IUserService userService,
            final @NotNull ITaskService taskService,
            final @NotNull IProjectService projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @GetMapping("")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/task-create");
        modelAndView.addObject("users", userService.findAll());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("task") final @NotNull Task task,
            @RequestParam(value = "userId") final @NotNull  String userId,
            @RequestParam(value = "projectId") final @NotNull  String projectId
    ) {
        taskService.create(userId, projectId, task.getName(), task.getDescription());
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") final @NotNull String id) {
        taskService.removeTaskById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable(value = "id") final @NotNull String id) {
        @Nullable final Task task = taskService.findTaskById(id);
        return new ModelAndView("/task/task-update", "task", task);
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("task") final @NotNull Task task) {
        taskService.updateTaskById(task.getId(), task.getName(), task.getDescription());
        return "redirect:/tasks";
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") @NotNull final String id) {
        @Nullable final Task task = taskService.findTaskById(id);
        return new ModelAndView("/task/task-view", "task", task);
    }
}
